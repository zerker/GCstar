# GCstar translation

Thanks to several contributors, GCstar is available in many languages in addition to English.

The GCstar site has a [documentation page](http://wiki.gcstar.org/en/translators) on adding new languages or contributing missing translations.

Below is a set of links to files listing for each language the texts that are either missing (errors) or need to be checked as the translation is similar to English (warnings). There may be false positives if there is no difference from English. Feel free to contribute to the translation by proposing merge requests on Gitlab or posting your translation file in the [GCstar forum](http://forums.gcstar.org/) or contacting kerenoc01 on Google.

| Language    | Code | Translations to check |
| --------    | ---- | --------------------- |
| Arabic عربي | AR | [../tools/Translations_Checks/checkLang_AR.txt](../tools/Translations_Checks/checkLang_AR.txt) |
| Bulgarian   | BG | [../tools/Translations_Checks/checkLang_BG.txt](../tools/Translations_Checks/checkLang_BG.txt) |
| Català      | CA | [../tools/Translations_Checks/checkLang_CA.txt](../tools/Translations_Checks/checkLang_CA.txt) |
| Czech       | CS | [../tools/Translations_Checks/checkLang_CS.txt](../tools/Translations_Checks/checkLang_CS.txt) |
| Deutsch     | DE | [../tools/Translations_Checks/checkLang_DE.txt](../tools/Translations_Checks/checkLang_DE.txt) |
| Ελληνικά    | EL | [../tools/Translations_Checks/checkLang_EL.txt](../tools/Translations_Checks/checkLang_EL.txt) |
| Español     | ES | [../tools/Translations_Checks/checkLang_ES.txt](../tools/Translations_Checks/checkLang_ES.txt) |
| Français    | FR | [../tools/Translations_Checks/checkLang_FR.txt](../tools/Translations_Checks/checkLang_FR.txt) |
| Galego      | GL | [../tools/Translations_Checks/checkLang_GL.txt](../tools/Translations_Checks/checkLang_GL.txt) |
| Magyar      | HU | [../tools/Translations_Checks/checkLang_HU.txt](../tools/Translations_Checks/checkLang_HU.txt) |
| Indonesia   | ID | [../tools/Translations_Checks/checkLang_ID.txt](../tools/Translations_Checks/checkLang_ID.txt) |
| Italiano    | IT | [../tools/Translations_Checks/checkLang_IT.txt](../tools/Translations_Checks/checkLang_IT.txt) |
| Nederlands  | NL | [../tools/Translations_Checks/checkLang_NL.txt](../tools/Translations_Checks/checkLang_NL.txt) |
| Polski      | PL | [../tools/Translations_Checks/checkLang_PL.txt](../tools/Translations_Checks/checkLang_PL.txt) |
| Português   | PT | [../tools/Translations_Checks/checkLang_PT.txt](../tools/Translations_Checks/checkLang_PT.txt) |
| Română      | RO | [../tools/Translations_Checks/checkLang_RO.txt](../tools/Translations_Checks/checkLang_RO.txt) |
| Russian     | RU | [../tools/Translations_Checks/checkLang_RU.txt](../tools/Translations_Checks/checkLang_RU.txt) |
| Srpski      | SR | [../tools/Translations_Checks/checkLang_SR.txt](../tools/Translations_Checks/checkLang_SR.txt) |
| Svenska     | SV | [../tools/Translations_Checks/checkLang_SV.txt](../tools/Translations_Checks/checkLang_SV.txt) |
| Türkçe      | TR | [../tools/Translations_Checks/checkLang_TR.txt](../tools/Translations_Checks/checkLang_TR.txt) |
| Ukrainian   | UK | [../tools/Translations_Checks/checkLang_UK.txt](../tools/Translations_Checks/checkLang_UK.txt) |
| 中文(繁體)   | ZH | [../tools/Translations_Checks/checkLang_ZH.txt](../tools/Translations_Checks/checkLang_ZH.txt) |
| 中文(简体)   | ZH_CN | [../tools/Translations_Checks/checkLang_ZH_CN.txt](../tools/Translations_Checks/checkLang_ZH_CN.txt) |
