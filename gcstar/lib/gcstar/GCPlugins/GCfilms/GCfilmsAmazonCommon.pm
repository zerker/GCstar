package GCPlugins::GCfilms::GCfilmsAmazonCommon;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2019 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;

use GCPlugins::GCfilms::GCfilmsCommon;
use GCPlugins::GCstar::GCAmazonCommon;

{
    package GCPlugins::GCfilms::GCfilmsAmazonPluginsBase;

    use base ('GCPlugins::GCfilms::GCfilmsPluginsBase', 'GCPlugins::GCstar::GCPluginAmazonCommon');

    use GCUtils;

    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;

        $self->{inside}->{$tagname}++;

        if ($self->{parsingEnded})
        {
            if ($self->{itemIdx} < 0)
            {
                $self->{itemIdx} = 0;
                $self->{itemsList}[0]->{url} = $self->{loadedUrl};
            }
            return;
        }

        if ($self->{parsingList})
        {

            $self->{beginParsing} = 1
                if ($tagname eq 'span' && $attr->{'data-component-type'} =~ m/s-search-results/);

            return if ! $self->{beginParsing};

            if ($tagname eq 'a' && $attr->{class} =~ /a-link-normal a-text-normal/)
            {
                if ($self->{isSponsored} eq 1)
                {
                    $self->{isSponsored} = 2;
                    return;
                }
                elsif ($self->{isSponsored} eq 2)
                {
                    $self->{isSponsored} = 0;
                }
                $self->{itemIdx}++;
                $self->{itemsList}[$self->{itemIdx}]->{url} = "https://www.amazon.".$self->{suffix}.$attr->{href};
                $self->{isTitle} = 1;
            }
            return if $self->{isSponsored};
            if ($tagname eq 'span' && $attr->{'data-component-type'} =~ m/sp-sponsored-result/)
            {
            	$self->{isSponsored} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{'data-asin'})
            {
                $self->{isSponsored} = 0;
            }
            elsif ($tagname eq 'span' && $attr->{class} eq "a-size-small a-color-secondary")
            {
                $self->{isPublication} = 1
                    if ($self->{itemIdx} && !$self->{itemsList}[$self->{itemIdx}]->{date});
                $self->{isActors} = 1
                    if ($self->{itemIdx} && !$self->{itemsList}[$self->{itemIdx}]->{actors});
            }
        }
        else
        {
            if (($tagname eq "img") && ($attr->{id} eq 'landingImage') && (!$self->{curInfo}->{image}))
            {
		        if ($attr->{src} =~ m/data:image/)
		        {
		            # image sent in HTML (film 2010)
		            $self->{curInfo}->{image} = $attr->{"data-a-dynamic-image"} if ($attr->{"data-a-dynamic-image"});
		        }
		        $self->{curInfo}->{image} = $attr->{src} if (! ($attr->{src} =~ m/data:image/));
            }
            elsif ($attr->{'data-automation-id'} eq 'synopsis') # US Prime video
            {
                    $self->{insideSynopsis} = 2;
            }
            elsif ($tagname eq 'h2')
            {
                $self->{insideSynopsis} = 1
                    if (!$self->{curInfo}->{synopsis});
                $self->{insideOriginal} = 1
                    if (0 && !$self->{curInfo}->{original});
            }
            elsif (($tagname eq 'h3' || $tagname eq 'br'|| $tagname eq 'p') && $self->{insideSynopsis} eq 2)
            {
                $self->{curInfo}->{synopsis} .= "\n" if $self->{curInfo}->{synopsis};
            }
	        elsif (0 && ($tagname eq 'br' || $tagname eq 'a') && $self->{insideSynopsis} eq 2)
	        {
	           $self->{insideSynopsis} = 0;
	        }
            elsif ($tagname eq 'span' && $attr->{'data-automation-id'} =~  m/maturity-rating/ && ! $self->{curInfo}->{age})
            {
                $attr->{title} =~ s/\s*$//;
                $self->{curInfo}->{age} = $self->decodeAge($attr->{title});
            }
            elsif ($tagname eq 'div' && ($attr->{class} =~  m/bgimg-desktop/ || $attr->{class} =~  m/fallback-packshot/) )
            {
                $self->{insideImage} = 1;
            }
            elsif ($tagname eq 'div' && $attr->{class} =~ m/bgimg__div/ && $self->{insideImage})
            {
                if ($attr->{style} =~ m/url\((.*)\)/)
                {
                    $self->{curInfo}->{image} = $1;
                    $self->{insideImage} = 0;
                }
            }
            elsif ($tagname eq 'img' && $self->{insideImage})
            {
                $self->{curInfo}->{image} = $attr->{src};
                $self->{insideImage} = 0;
            }
            elsif ($tagname eq "span")
            {
		        $self->{insideGenre} = 1 if ($attr->{class} eq "zg_hrsr_ladder");
		        $self->{isTitle} = 1 if ($attr->{id} eq 'productTitle');
		        $self->{insideField} = 1 if ($attr->{class} =~ m/color-secondary/);
                $self->{insideDate} = 1 if ($attr->{'data-automation-id'} =~ m/release-year/); # US Prime video
            }
            elsif ($tagname eq "h1" && $attr->{'data-automation-id'} eq "title")
            {
                # US Prime video
                $self->{isTitle} = 1;
            }
            elsif ($tagname eq 'li' && $attr->{class} eq "swatchElement selected")
            {
                $self->{insideFormat} = 1;
            }
            elsif ($tagname eq 'i' && $attr->{class} =~ m/a-icon-star/ && ! $self->{curInfo}->{ratingpress})
            {
                $self->{insideRatingPress} = 1;
            }
        }
    }

    sub end
    {
        my ($self, $tagname) = @_;

        $self->{inside}->{$tagname}--;
        if ($tagname eq "li")
        {
            $self->{insideActors} = 0;
            $self->{insideDirector} = 0;
        }
	    elsif ($tagname eq 'div' && $self->{insideSynopsis} eq 2)
	    {
	        $self->{insideSynopsis} = 0;
	    }
	    elsif ($tagname eq 'h2' && $self->{insideSynopsis} eq 1)
	    {
	        $self->{insideSynopsis} = 0;
	    }
        elsif ($tagname eq 'h3' && $self->{insideSynopsis} eq 2)
        {
            $self->{curInfo}->{synopsis} .= "\n";
        }
	    elsif ($tagname eq 'span')
	    {
            $self->{insideGenre} = 0;
	    }
	    elsif ($tagname eq 'dd')   # US Prime Video
	    {
            $self->{insideGenre} = 0;
	        $self->{insideDirector} = 0;
	        $self->{insideActors} = 0;
	    }
    }

    sub text
    {
        my ($self, $origtext) = @_;

        $origtext =~ s/[\n\s]*$//s;
        $origtext =~ s/^[\n\s]*//s;
        return if $origtext eq '';

        if ($self->{parsingList})
        {
            return if ! $self->{beginParsing};

            if ($self->{isTitle})
            {
            	$self->{itemsList}[$self->{itemIdx}]->{title} = $origtext;
            	$self->{isTitle} = 0;
            }
            elsif (($self->{inside}->{title})
             && ($origtext !~ /^$self->{translations}->{site}/))
            {
                $self->{parsingEnded} = 1;
            }
            elsif ($self->{isPublication} eq 1 && $origtext =~ m/^[0-9]{4}$/)
            {
                $origtext =~ m/([0-9]{4})/;
                $self->{itemsList}[$self->{itemIdx}]->{date} = $1;
                $self->{isPublication} = 0;
                return;
            }
            elsif ($self->{isActors} eq 1)
	        {
	           $self->{isActors} = 2 if ($origtext =~ m/$self->{translations}->{distribution}/);
	        }
            elsif ($self->{isActors} eq 2)
            {
                $self->{itemsList}[$self->{itemIdx}]->{actors} = $origtext
                    if ! $self->{itemsList}[$self->{itemIdx}]->{actors};
                $self->{isActors} = 0;
                return;
            }
            elsif ($origtext =~ m/^$self->{translations}->{sponsored}/)
            {
                $self->{isSponsored} = 1;
            }
        }
        else
        {
            $origtext =~ s/\s{2,}/ /g;
            $origtext =~ s/\s+,/,/;
            $origtext =~ s/^\s*//;
            $origtext =~ s/\s*$//;

            if (($self->{insideActors}) && ($origtext !~ /^,/))
            {
                if ($self->{actorsCounter} < $GCPlugins::GCfilms::GCfilmsCommon::MAX_ACTORS)
                {
                    push @{$self->{curInfo}->{actors}}, [$origtext];
                    $self->{actorsCounter}++;
                }
            }
            elsif (($self->{insideDirector}) && ($origtext !~ /^,/))
            {
                $origtext =~ s/,.$//;
                $self->{curInfo}->{director} .= ", "
                    if $self->{curInfo}->{director};
                $self->{curInfo}->{director} .= $origtext;
            }
            elsif (($self->{insideTime} || $origtext =~ m/\d+ min/) && ! $self->{curInfo}->{time})
            {
                $self->{curInfo}->{time} = $origtext;
                $self->{curInfo}->{time} =~ s/ [a-z]+//i;
                $self->{curInfo}->{time} =~ s/\..*//i;
                $self->{insideTime} = 0;
            }
            elsif ($self->{insideDate})
            {
                $origtext =~ s/\-$//;
                $self->{curInfo}->{date} = $self->decodeDate($origtext);
                $self->{insideDate} = 0;
            }
            elsif ($self->{insideVideo})
            {
                $self->{curInfo}->{video} = $origtext;
                $self->{insideVideo} = 0;
            }
            elsif ($self->{insideFormat})
            {
                $self->{curInfo}->{format} = $origtext;
                $self->{insideFormat} = 0;
            }
            elsif (($self->{insideOriginal} eq 1) && ($origtext =~ m/$self->{translations}->{description}/i ))
            {
                $self->{insideOriginal} = 2;
            }
            elsif (($self->{insideOriginal} eq 2) && ($origtext ne ''))
            {
		        if ($origtext =~ m/\(.*\),.*,.*$self->{translations}->{minutes}/)
		        {
		            $origtext =~ s/.*\(//;
		            $origtext =~ s/\).*//;
		            $origtext =~ s/\..*//;
		            $self->{curInfo}->{original} .= $origtext;
		        }
		        $self->{insideOriginal} = 0;
            }
            elsif (($self->{insideSynopsis} eq 1) && ($origtext eq 'Synopsis' || $origtext eq $self->{translations}->{description}))
            {
                $self->{insideSynopsis} = 2;
            }
            elsif (($self->{insideSynopsis} eq 2) && ($origtext ne ''))
            {
                $self->{curInfo}->{synopsis} .= $origtext;
            }
            elsif ($self->{insideAudio})
            {
                $origtext =~ s/^\s//;
                $self->{curInfo}->{audio} = $origtext;
                $self->{insideAudio} = 0;
            }
            elsif ($self->{insideSubTitle})
            {
                $origtext =~ s/^\s//;
                $self->{curInfo}->{subt} = $origtext;
                $self->{insideSubTitle} = 0;
            }
	        elsif ($self->{insideGenre} eq 1)
	        {
		        if (! ($origtext =~ m/^Blu-ray/i || $origtext =~ m/[>,]/ || $origtext =~ m/^$self->{translations}->{in}/))
		        {
		            $origtext =~ s/\s*\(DVD.*Blu-ray\)//;
		            $origtext = ", ".$origtext if ($self->{curInfo}->{genre});
		            $self->{curInfo}->{genre} .= $origtext;
		        }
	        }
            elsif ($self->{insideAge} eq 1 && ! $self->{curInfo}->{age})
            {
                $self->{curInfo}->{age} = $self->decodeAge($origtext);
                $self->{insideAge} = 0;
            }
	        elsif ($self->{isTitle} eq 1)
	        {
		        $self->{curInfo}->{title} = $origtext;
		        $self->{isTitle} = 0;
	        }
	        elsif ($self->{insideRatingPress})
	        {
	            return if (! ($origtext =~ m/$self->{translations}->{stars}/));
                $origtext =~ s/\s*$self->{translations}->{stars}//;
                $origtext =~ s/,/./;
                $self->{curInfo}->{ratingpress} = $origtext * 2;
                $self->{insideRatingPress} = 0;
	        }
            elsif ($self->{inside}->{b} || $self->{inside}->{dt} || $self->{insideField})
            {
                $self->{insideActors}   = 1 if $origtext =~ /^$self->{translations}->{actors}\s*:*/;
                $self->{insideDirector} = 1 if $origtext =~ /^$self->{translations}->{director}\s*:*/;
                $self->{insideDate}     = 1 if $origtext =~ /^$self->{translations}->{date}.*:/i;
                $self->{insideTime}     = 1 if $origtext =~ /^$self->{translations}->{duration}\s*:/i;
                $self->{insideAudio}    = 1 if $origtext =~ /^$self->{translations}->{audio}\s*:/;
                $self->{insideSubTitle} = 1 if $origtext =~ /^$self->{translations}->{subtitles}\s*:/;
                $self->{insideVideo}    = 1 if $origtext =~ /^$self->{translations}->{video}\s*:/;
                $self->{insideGenre}    = 1 if $origtext =~ /^$self->{translations}->{genre}\s*/;
                $self->{insideAge}      = 1 if $origtext =~ /^$self->{translations}->{age}\s*/;
                $self->{insideField} = 0;
            }	
        }
    }

    sub preProcess
    {
        my ($self, $html) = @_;

        $html = $self->SUPER::preProcess($html);

        $self->{insideActors} = 0;
        $self->{insideDirector} = 0;
        $self->{insideDate} = 0;
        $self->{insideTime} = 0;
        $self->{insideAudio} = 0;
        $self->{insideSubtitle} = 0;
        $self->{insideFormat} = 0;
        $self->{insideOriginal} = 0;
        $self->{insideGenre} = 0;
        $self->{insideVideo} = 0;
        $self->{insideFormat} = 0;
        $self->{insideSynopsis} = 0;
        $self->{insideRatingPress} = 0;
        $self->{insideImage} = 0;
        $self->{insideAge} = 0;
        $self->{insideField} = 0;

        if ($self->{parsingList})
        {
            $html =~ s|~(.*?)<span class="bindingBlock">\(<span class="binding">(.*?)</span>( - .*?[0-9]{4})?\)</span>|<actors>$1</actors><format>$2</format><publication>$3</publication>|gsm;
        }
        else
        {
            # problem when an image is embedded in the HTML (film 2010)
            # the attribute data-a-dynamic-image="{&quote;http would return {
            $html =~ s/data-a-dynamic-image="\{&quot;/data-a-dynamic-image="/;
        }

        $self->{parsingEnded} = 0;
        $self->{currentRetrieved} = '';
        $self->{beginParsing} = 0;

        return $html;
    }

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{codeField} = '';
        $self->{searchType} = 'dvd';

        return $self;
    }

    sub getSearchFieldsArray
    {
        return ['title','ean'];
    }

    sub getEanField
    {
        return 'ean';
    }
    
    sub decodeAge()
    {
        my ($self, $age) = @_;
        
        my $resultAge;
        $resultAge = 2 if $age =~ m/^$self->{translations}->{ratedG}/i;
        $resultAge = 5 if $age =~ m/^$self->{translations}->{ratedPG}/i;
        $resultAge = 13 if $age =~ m/^$self->{translations}->{ratedPG13}/i;
        $resultAge = 18 if $age =~ m/^$self->{translations}->{ratedR}/i;
        $resultAge = $1 if ($age =~ m/(\d+)/);
        return $resultAge;
    }

    sub decodeDate
    {
        my ($self, $date) = @_;

        # date déjà dans le bon format
        return $date if ($date =~ m|/.*/|);
        # année seule
        return "01/01/".$date if ($date =~ m/^[0-9]+$/);
        # pas d'année
        return '' if (! $date =~ m/[0-9][0-9][0-9][0-9]/);
        if ($date =~ m/trimestre/)
        {
            $date =~ s/[^0-9]* *trimestre */ /;
            my @dateItems = split(/\s/, $date);
            $date = "01/01/".$dateItems[1] if ($dateItems[0] eq '1');
            $date = "01/04/".$dateItems[1] if ($dateItems[0] eq '2');
            $date = "01/07/".$dateItems[1] if ($dateItems[0] eq '3');
            $date = "01/10/".$dateItems[1] if ($dateItems[0] eq '4');
            return $date;
        }
        $date = GCUtils::strToTime($date,"%e %B %Y",$self->getLang());
        $date = GCUtils::strToTime($date,"%e %b %Y",$self->getLang());
        $date = GCUtils::strToTime($date,"%B %e %Y",$self->getLang());
        $date = GCUtils::strToTime($date,"%b %e %Y",$self->getLang());
        return  GCUtils::strToTime($date,"%B %Y",$self->getLang());
    }
}

1;
